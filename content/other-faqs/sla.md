---
title: SLA
weight: 14
---

# Service Level Agreement

Evercam is committed to delivering exceptional service, and to partnering with our Clients to 
ensure the successful deployment and use of Evercam’s solutions and services. We continually 
review our practices to improve internal operations and deliver a service that meets our 
Clients' needs.

Evercam understands that its Clients depend on the reliable installation and maintenance of the 
hardware and software that are supplied (unless otherwise stipulated) and maintained by Evercam.

**Our Customer Support Team looks after:**
- Proactively troubleshooting Offline Camera Events 
- Responding to Customer Tickets
- Footage retrieval requests
- Software maintenance and updates
- Transferring of tickets to Site Operations if a site visit is required.
- Product feedback mechanism
- Arrange training session(s) with our Customer Success Team.

**Our Site Operations Team looks after:** 
- Installations
- Commissioning
- Service/Maintenance Visits
- Camera Relocations
- Decommissioning

## Support Ticket Workflow

<p align="center">
<img src="/images/support_ticket_workflow.png" width = 600 />
</p>

## Overview

| Status | Priority | Response Time | Example Issue Descriptions | Resolution Time* |
| ------| ------ |  ------ | ------ |  ------|
| Critical | Priority 1 |  2 hours | Evercam Platform not accessible. Footage request due to a serious Health and Safety Incident. |  8 hours |
| Standard | Priority 2 |  4 hours | Camera(s) offline. Standard Footage request |  1 Business Day |
| Change | Priority 3 |  1 Business Day | Add new user, Camera relocation request. Amend camera details |  1 Business Day |

**Onsite Resolution**
For issues requiring on-site resolution, Evercam endeavors to have a date and time for a site 
visit agreed with the Client within 48hrs of receiving the case.

Evercam operates on the basis that there will be a Client representative onsite that can 
assist the Evercam Support team with preliminary troubleshooting. 

**How to contact our Support Team**

Customer queries can be logged 24 hours a day, seven days a week, 365 days a year via 
support@evercam.io  Please use this email address in addition to, or in place of your Evercam 
Account Manager’s address for the fastest response. Emailing support@evercam.io is the most 
efficient way to open a ticket.

Alternative means of raising a ticket include contacting our customer success team, your 
account manager, or via our chatbot on our landing page.

All incoming customer enquiries are answered directly by our support team with support 
management monitoring open incidents and making available appropriate resources to facilitate 
the resolution of support cases. This process provides a formal mechanism to deal with more 
complex issues and ensures that the Evercam high standards of customer service are maintained.

As part of our continuous improvement plan we are always keen to understand how our Clients 
feel about the service received when interacting with Evercam. Every time we close a case, we 
ask for feedback on the service you’ve received from the Evercam support teams. This is used to 
monitor and evaluate the service we deliver and to ensure we improve your experience with 
Evercam.

## Camera Status, Offline Events and Response Timelines

**Camera Status**
There are 4 main camera statuses on the Evercam system:

1. Offline
- The camera is offline and has a fault that has not been recognised by, or reported to, Evercam
2. Offline (Scheduled)
- The camera is on an agreed power schedule whereby the camera will be offline and inaccessible 
for agreed timeslots. 
- *Please Note*: During the periods that a camera is scheduled to be offline, the camera will 
not record either to the cloud or locally.
3. Under Maintenance
- The camera is offline and is being investigated by the Evercam Support team.
4. Under Maintenance waiting for site visit
- The camera is offline and the case has been transferred to the Site Operations team as it may 
require a site visit to be scheduled.

## Offline Events 

While Evercam’s hardware and software solutions are extremely reliable, offline events can 
arise. The majority of camera office events can be resolved remotely or with the assistance of 
an onsite Client representative. 

Below is a list of the main faults and the typical resolution steps.

| Offline Event | Resolution |
| ------| ------ |
| Loss of broadband connectivity | Reboot router remotely |
| Loss of Power | Monitor for a period then confirm with Client that power is available onsite.|
| Hardware Failure | Reboot, diagnose, confirm Camera & NVR are connected. If required, schedule a site visit. |
| Onsite damage to Hardware or Cabling | Remotely confirm which devices are disconnected or damaged. Evercam will request onsite Client assistance to conduct a visual inspection & photos if physical damage is suspected before scheduling a visit. |

Given the multiple possible causes of failure of the camera and recording system, it is 
understood that neither Evercam nor its partners will be held liable for any loss of recordings 
or losses due to missing recordings, for whatever reason.

A site visit, as well as any associated hardware replacement or access costs, will be deemed 
chargeable to the Client if it is arising from events beyond the control of Evercam, including 
but not limited to, onsite damage, failure to provide power, acts of God, etc. 

## Response Times for Offline Events

Evercam Customer Support will endeavour to restore an offline camera remotely, or with the 
assistance of an onsite Client representative, within 12hrs of becoming aware of the fault.

If a site visit is required, the Evercam Site Operations team endeavours to have agreed a date 
and time with the Client within 48hrs of receiving the case for the Evercam Customer Support 
team.

Site visits that are not arising from a failure of the Evercam hardware or software will be 
deemed chargeable to the Client.

## Footage Requests

Evercam will endeavour to process any High Resolution Footage request within 12 working hrs. 
This timeline is dependent on the length of footage requested and the local broadband 
connectivity.

A footage request based on the Cloud Recordings can be processed while the High Resolution 
Footage is being processed.

## Uptime

Evercam expects the Evercam Platform to have an uptime rate of 98% (or higher).
The uptime rate of a camera(s) is subject to the unique variables on each site including, but 
not limited to, power supply, connectivity and onsite activity. 

## Client Responsibilities and Assumptions

- The Client will make available personnel with the necessary time and resources to ensure the 
successful installation and operation of the Evercam System. 
- The Client will ensure easy access to the camera mounting location(s) as well as providing 
power, if required.
- Management of any 3rd party, excluding those engaged by Evercam, remains the responsibility 
of the Client. 
- Evercam expects timely engagement from the Client and any Client 3rd parties. Failure to do 
so may incur additional costs for the Client which Evercam will not accept liability for.
- The Client will not tamper/move the equipment without prior notice given to Evercam
- The Client will ensure a stable and reliable provision of power, either in the form of a 
mains power supply or a generator connection. 
    - If the camera is being powered by a generator, the Client will ensure that power is 
    provided as per the agreed schedule.
- If the camera is being powered by Solar panels, the Client will ensure that the panels and 
batteries are not damaged or moved, without prior consultation with Evercam. The Client will 
also ensure that the panels are kept free from debris, dust and over-growth.
- It is also assumed that the Client understands the limitations of Solar as a power source 
that includes, but is not limited to:
    - The requirement for the camera to operate to a power schedule outside which the camera 
    will not be recording.
    - The requirement for adequate sunshine
    - The requirement to ensure there are no blockages of sunlight to the panels, either 
    permanent or temporary, as they could impair the performance of the solar panels.
    - The potential for reduced recording capabilities.
- Failure by the Client to meet the assumptions outlined above for the operation of either 
generator or solar kits may result in the replacement of batteries which will be chargeable to 
the Client.
- The Client will inform Evercam of any changes on site that may affect the performance of the 
Evercam system. Examples include, but are not limited to,
    - Erection of scaffolding in front of a camera
    - Loss of generator power
    - Blockage of light to the solar panels
- The Client will take all reasonable steps to ensure that Evercam hardware is not interfered 
with, either accidentally or intentionally. Any site visits, including the replacement of 
hardware, that are not a failure of Evercam hardware will be deemed charged by Evercam.
- Where the camera is hosted by a third party building Evercam:
    - cannot guarantee the provision of physical hosting of the camera for the duration of the 
    project.
    - cannot guarantee that there will not be delays in gaining access to the Host Building to 
    resolve any offline camera events.
- Any Client nominated representatives are assumed to have the necessary authority to engage 
with Evercam.
- Evercam will not accept liability for any offline events, damage to hardware or requirements 
for site visits arising from Acts of God, such as adverse weather conditions, fires, national 
emergencies, pandemics, strikes, shortage of labour or equipment or any other delays causing 
business interruption beyond the power or control of Evercam.
- Instructions to Evercam to produce incident footage, change contract duration, add or remove 
users from a Project, or other changes to previously agreed settings on the cameras, should be 
provided in writing by the Client. 
- It is assumed that the Client will meet all payments terms and conditions (30 
- Evercam will not accept liability arising from a Client failing to meet any of the 
assumptions outlined above and reserve the right to recoup any costs for these failings, should 
they arise. 
- All Data SIM cards that are provided by the client must be to the specification required (as 
communicated by Evercam Operation Team for the specific application and region).

**Gate Report**
- Camera Field of View, position of gate, direction of traffic, confirmation of unobstructed 
view of vehicles will all be agreed in advance of calibration of the Gate Report Camera.  
Changes to these may result in the Gate Report not functioning properly and may require 
intervention on site by the client or by Evercam, at the clients cost. 

**BIM Integration**
- Access to correct up to date models will be required by Evercam, and changes to the models or 
repositioning of the camera may result in the BIM Model feature not functioning properly and 
may require intervention on site by the client or by Evercam, at the clients cost.     

**GDPR**
- Evercam is the Data Processor, the client is the Data Controller. 

**Data Storage**
- The physical storage medium will be retrieved and stored by Evercam for the period agreed 
with the Client and in accordance with any relevant legislation. 
- Evercam may use recordings for the purposes of product improvements & algorithm training

## Hardware Warranties

Any warranties listed below are those provided by the manufacturer. Evercam will not accept 
liability for any hardware failure outside of the warranty period and conditions set-out by the 
manufacturer.

| Product | Description |
| ------| ------ |
| Camera | 5 years |
| Hard Drive | 2 years |
| Batteries | 1 year |
| NVR or SBC | 3 year |
| Router | 2 years |

