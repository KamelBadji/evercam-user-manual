---
title: Security
weight: 11
---

# Security / Compliance

### **Does Evercam sit on our network? Is it secure?**
No, it does not sit on your network. Yes, it is secure. Evercam runs on an external cellular mobile network that does not touch UPS internal servers.

### **Do I need to provide an internet connection?**
No. Evercam Camera systems ship with built-in 4G LTE cellular modems & data plans. When the 
camera is powered on, it connects to Evercam service automatically. 

### **Where do we cloud-host our customer data?**
Locally on-site using encrypted hard drives & in a ISO 27001 certified data centre.

Hetzner hosts our cloud infrastructure, using servers located in Germany. In terms of data 
centre security, Hetzner’s policies can be found here:
https://www.hetzner.de/pdf/en/Sicherheit_en.pdf   

### **Who has access to recordings and live feed?** 
Only the camera owner including the list of users that the customer may add and Evercam 
employees with a business need to access our systems are granted access.

### **What safeguards are in place to protect the surveillance system from cyber-attacks**
End-to-end encryption via VPN.
 
### **Who do you share recordings with?**
Access to recordings is shared with Evercam registered users only.

### **How do you share recordings with authorised agencies and parties?** 
Authorised users access via secure login on Evercam dashboard. 

### **When data is shared to/from the system, how is this accomplished?**
Via Evercam dashboard interface for authorised users only.
 
### **Is the shared data encrypted in transit?**
VPN is used whenever data needs to be transferred.
