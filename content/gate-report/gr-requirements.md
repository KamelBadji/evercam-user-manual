---
title: Requirements
weight: 7
---

# Gate Report Requirements

1. Operations:
- NVR (Network Video Recorder) is mandatory.
- Stable Main power is highly advisable. Battery systems involved consume a lot.
- Good internet connection with fair amount of data (no less than 1 TB/month) is needed.


2. Region Of Interest:
- Trailer side should be visible to distinguish between trucks that look similar from the front.
- Trailer top should be visible to show what is inside the truck.
- Clear & visible entrance with no occlusions (trees, cranes, people, containers, etc).  
- Between 1.5 and 2 truck lengths should fit inside the ROI.
- Should be approved by Gate Report Manager before finalizing installation.

