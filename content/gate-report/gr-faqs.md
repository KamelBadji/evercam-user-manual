---
title: FAQs
weight: 8
---

# Gate Report FAQs

### **What vehicles does it track?**
- Flatbed semi-trailer
- Concrete Truck
- Small Truck
- Tipping Truck
- Tank Truck
- Concrete pump
- Road vehicle
- Semi-trailer

### **When is Gate Report most relevant?**
- Demolition / Muckaway at the beginning of a project
- Concrete Pours at the middle of a project
- Modular / Precast / OSM arriving at the end of a project 

### **What is Gate Report for?**
- Tracking vehicles going in and out of the site
- Verifying information from other sources

### **How accurate is the Gate Report?**
Evercam aims to achieve **>95%** accuracy. Mistakes can occur due to conditions outside of Evercam’s control.