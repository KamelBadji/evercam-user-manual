---
title: User Manual
type: docs
---

<img src="/images/EVERCAM_logo_Transparent.png" width = 50 /> 

# Purpose

This manual provides a reference on how to use the 
features and products within the platform.
