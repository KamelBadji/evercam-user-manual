---
title: Requirements
weight: 2
---

# Drone Requirements

2 Cases :

1. You have your own drone and send us the drone images or the model via Drone Deploy 
(obj) , Pix4D (obj / las), Reconstruct, ContextCapture…etc. as a Textured Mesh model 
(preferable to Point Cloud model).

2. You don't have a drone and ask for Drone : We send a subcontractor on site to do 
the mapping, and get the drone files.

## Flight Requirements

**Suitable height for the drone when taking the images for better results:**
It depends on the site and height of tallest structure on site, if it’s a building site with no cranes etc then usually around 60m, if there is a crane at 50-60m then go approx 20m above it so 80-90m.

Usually pictures are taken at one height. 

**Recommended photo spacing interval:**
Spacing is controlled using the drone path mapping software. Different providers have different 
settings.

**Angle required for taking the pictures:**
For large sites, take them straight down. If it's a small structure like a bridge, then set the 
angle at 65 degrees and fly the perimeter facing in to the centre of the site also.

