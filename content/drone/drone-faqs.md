---
title: FAQs
weight: 3
---

# Drone FAQs

## General

### **How frequent should the flights be?**
This depends on how often you want the images updated.

### **Do I need a BIM Model for Drone?**
A BIM Model is not necessary to create a Drone model, but if the client wants to integrate the BIM for the compare feature, then the BIM Model should be shared.

### **How accurate is the measuring tool?**
Our measuring tool has a margin of error of (+/-)5mm. Drone works entirely detached, and no architectural drawings or BIM is needed for distances or area measuring to work.

## Drone Mapping

### **What height gives the best results?**
This depends on the site and height of tallest structure on site:
- If it is a building site with no cranes, it is better to use 50m to 60m.
- If there is a crane at 50-60m, then go 20m above it (around 80m to 90m).

**Note: All photos need to be taken at the same height.**

### **Is there an angle required?**
To improve the accuracy and quality of the model, we recommend using a 65° angle.

### **How many photos should be taken?**
This depends on the site's area, if it’s:
Less than 30,000 m² ⇒ 100 to 250 photos.
Between 30,000 m² and 60.000 m² ⇒ 250 to 400 photos.
Between 60,000 m² and 100.000 m² ⇒ 400 to 600 photos.
More than 100,000 m² ⇒ +600 photos.

**Note: All photos should be taken in one mapping (multiple mappings gives bad results).**


### **What about Flight Planning?**

We usually recommend an evenly spread out flight path to get details in an equal pattern (like 
the image below).

A single flight at a fixed height is highly recommended. Different heights will cause 
processing issues and the flight might have to be redone.

It is okay to have a higher density of images around important structures on site, as long as 
the height is close to the altitude of the remaining bits of the flight.

<p align="center">
<img src="/images/flight_mapping.png" width = 600 />
</p>
