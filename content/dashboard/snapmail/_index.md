---
title: Snapmail
weight: 4
---

# Snapmail

{{< hint info >}}
Semi-Automate documenting & sharing progress photos by scheduling a daily/weekly email of an updated progress image to an email inbox and/or documentation folder.
{{< /hint >}}

<p align="center">
<img src="/images/snapmail.png" width = 600 />
</p>

This tool can also be used to share images to others without sharing access to the camera or the project.

## How to Use

1. If you want to schedule a Snapmail, click on *Snapmail* at the left navigation bar.

2. From there, you can click on the *Create a Snapmail* button.

<p align="center">
<img src="/images/new_snapmail_2.png" width = 400 />
</p>

3. Then you can choose your preferred day and time to receive a snapshot from a camera.

4. You'll have to enter an email address and then you’re all set!

5. You can review, edit, or pause your created snapmails at any time.

<p align="center">
<img src="/images/snapmail_view.png" width = 800 />
</p>

{{< hint danger >}}
Watch the [Snapmail Tutorial](https://vimeo.com/showcase/8254136/video/522348434) for more information.
{{< /hint >}}