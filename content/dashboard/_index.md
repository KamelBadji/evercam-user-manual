---
title: Dashboard
weight: 1
bookCollapseSection: true
---

# Dashboard

The dashboard gives an overview of all the Evercam products and features.

<p align="center">
<img src="/images/project_dashboard.png" width = 600 />
</p>

## Subtopics

- [Account]({{< relref "/dashboard/account" >}})
- [Company Admin]({{< relref "/dashboard/company-admin" >}})
- [Video Wall]({{< relref "/dashboard/video-wall" >}})
- [Snapmail]({{< relref "/dashboard/snapmail" >}})
- [Archives]({{< relref "/dashboard/archives" >}})
- [Integrations]({{< relref "/dashboard/integrations" >}})