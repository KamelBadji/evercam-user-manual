---
title: Integrations
type: docs
weight: 6
bookFlatSection: true
---

# Integrations

{{< hint info >}}
An Integration is a tool Evercam builds to connect and communicate to other construction 
software programs, allowing easier access between programs or transfer of data and 
documentation between the programs.
{{< /hint >}}

<p align="center">
<img src="/images/integration_view.png" width = 800 />
</p>

## Embedded Live View

Our Integrations allow for an Embedded Live View, where you can see your standard Evercam 
Live View, directly in your project management program.

## Documentation

Integrations allow you to utilize Snapmail to automate transfer of Evercam images into a 
folder of your choosing on your project management or reality capture software.  

Our Integrations allow you to use snapmail to automate images to be sent on whatever time 
frame you desire.

## Full Usability

Full Usability allows the use of *Evercam*, including all of its functionality, while signed into 
another program.

Currently this is available only with *Procore* and *Autodesk*.

## How to set up an Integration? 

Within Evercam, you can click on *Settings*, and then *Integrations* to connect Procore or 
Aconex on your own. 

For all other integrations, you may reach out to integrations@evercam.io.