---
title: Aconex
type: docs
weight: 7
bookFlatSection: false
---

# Aconex

<p align="center">
<img src="/images/aconex.png" width = 200 />
</p>

**What is Aconex?**

It is a Construction Project Management Platform.

**How does it work?**

1. Go to *Settings > Integrations* within dash.evercam.io
2. Connect your Aconex account.
3. Go to *Snapmails > New Snapmail*.
4. Select *Aconex* as the provider and follow the steps.

## Enabling the Aconex Integration

1. Go to *Settings  >Integrations*.
2. Click Connect.
3. Pick your region.

<p align="center">
<img src="/images/aconex_integration.gif" width = 400 />
</p>

**Connecting to your Aconex account**

Verify your account: If not already logged in to Aconex, you will need to authenticate. You 
will later be redirected to the Evercam Dashboard.

<p align="center">
<img src="/images/aconex_account.gif" width = 400 />
</p>

**Confirmation**

Once you see the success notification at the top, you are now connected to Aconex and can use it in Snapmail.

Now, you can setup Snapmail for automatic capture of the Project Site within Aconex.

<p align="center">
<img src="/images/aconex_confirmation.gif" width = 400 />
</p>

## Create an Aconex Snapmail

Aconex Snapmail creation is as straightforward as with other Snapmail providers.

1. Go to *Snapmails > Create/Edit*.
2. Select *Aconex* as the provider.
3. Select the project you wish to save the snapmail to.
4. Fill the required fields.

<p align="center">
<img src="/images/aconex_snapmail.gif" width = 400 />
</p>

## Finding your image files in Aconex

Snapmail will be uploaded to Aconex and can be found under *Aconex > Documents*.

<p align="center">
<img src="/images/aconex_documents.png" width = 400 />
</p>