---
title: Requirements
weight: 6
---

# Requirements

## Hardware

### Camera & Accessories
- Camera: we recommend using (Insta360 ONE RS 1-Inch).
- Helmet Mount (any type should work, ex: GoPro).

### What parameters should I use?
- File type: Standard Video.
- Camera: we recommend using (Insta360 ONE RS 1-Inch).
- Resolution: 4K (3840x1920).
- Frame rate: 25fps or 30fps
- Duration: (between 20sec & 5min).

### How can I capture a good video?
- Use a helmet mount instead of a stick (to stabilise camera & avoid having blurry images).
    - If you don’t have a helmet mount, try to put the camera on your head to avoid your body covering 30-50% of the image.
- Maintain a constant walking speed throughout the walkthrough.
- If you have more than 1 floor, create multiple videos (one for each floor).

### What should I share with you?
After you’re done capturing the walkthroughs, each video will generate 2 *.insv* files:
- LRV (Low-Resolution Video): is used to preview the video.
- VID: is the original 360 video with all the data.

**You’ll need to share with us both files (Optional: Mention the name of each floor corresponding to the files) in order for us to create the 360.**