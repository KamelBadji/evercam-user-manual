---
title: 360
weight: 5
bookCollapseSection: true
---

# 360

{{< hint info >}}
360 allows you to visualize an interior or exterior space from all angles using mesh data.
{{< /hint >}}

<p align="center">
<img src="/images/360.png" width = 600 />
</p>

By providing the interior view, you can have full visibility of your construction project inside and out.

## Features

1. You can explore any space by clicking on the floor markers, which will bring you to that specific spot.

<p align="center">
<img src="/images/360_floor_markers.png" width = 600 />
</p>

2. You can measure distances on any surface as 360 recognizes spatial geometry. 

<p align="center">
<img src="/images/360_measure.png" width = 600 />
</p>

3. You can compare between 2 different dates at a time, or between actual progress and plans.

<p align="center">
<img src="/images/360_compare.png" width = 600 />
</p>

4. You can add tags to objects or views for other users to read. This is a helpful communication tool among users.

<p align="center">
<img src="/images/360_features.png" width = 600 />
</p>

5. You can open Live views of indoor cameras by clicking on the camera marker.

<p align="center">
<img src="/images/360_camera.png" width = 600 />
</p>

6. You can select among available walkthrough dates and floors of a project.

<p align="center">
<img src="/images/360_date_and_floors.png" width = 600 />
</p>