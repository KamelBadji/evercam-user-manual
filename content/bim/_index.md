---
title: BIM
weight: 3
bookCollapseSection: true
---

# BIM

{{< hint info >}}
BIM allows you to view a 3D or 4D digital model in its direct environment and 
switch between the model and reality to track changes and communicate progress.
{{< /hint >}}

<p align="center">
<img src="/images/bim.png" width = 600 />
</p>

BIM stands for Building Information Modeling. It is a digital representation and management of 
the physical and functional characteristics of a building or infrastructure. BIM is a 
collaborative process that involves the creation and use of intelligent 3D models to support 
the design, construction, and operation of buildings and other structures.

In a BIM model, various elements of the building, such as walls, floors, doors, windows, 
plumbing systems, electrical systems, etc., are represented as objects with detailed 
information attached to them. This information can include dimensions, materials, cost data, 
performance characteristics, and more.

## Features of BIM

<p align="center">
<img src="/images/bim_features.png" width = 600 />
</p>


1. If you want to access all Evercam features through the live camera view, click on any of the camera markers. 

<p align="center">
<img src="/images/bim_camera.png" width = 600 />
</p>

2. If you want to explore around the model, use the navigation tools to zoom in/out & rotate around the site.

<p align="center">
<img src="/images/bim_nav.gif" width = 600 />
</p>

3. If you want to check distances and areas, use the *Measure* tool.

<p align="center">
<img src="/images/bim_measurements.png" width = 600 />
</p>

4. If you want to highlight information for other users, use the *Tagging* tool to create annotations on the model.

<p align="center">
<img src="/images/tagging.gif" width = 600 />
</p>

5. If you would like to check out the surrounding buildings, toggle the *OSM buildings* feature on the upper right of the viewer.

<p align="center">
<img src="/images/osm_buildings.gif" width = 600 />
</p>

6. If you would like to check the weather conditions of the day or any of the previous days to see how the wather has affected the progress on site, toggle the *Weather* tool on the upper right of the viewer.

<p align="center">
<img src="/images/bim_weather.gif" width = 600 />
</p>

7. If you would like to create markups on the current view, use the *Edit* tool to take a screenshot, add information, and send the image to other stakeholders.

<p align="center">
<img src="/images/edit_tool.gif" width = 600 />
</p>

8. If you would like to inspect the BIM Model, you can select an element and use the *Hide*, *Isolate* or *Emphasize* tools in the *Settings* panel, as well as read information about them in *Item Details*.

<p align="center">
<img src="/images/bim_elements.gif" width = 600 />
</p>

9. If you would like to see the project milestones, use the *Timeline* bar at the bottom of the screen. This can also be hidden in the *Settings* panel.

<p align="center">
<img src="/images/timeline.gif" width = 600 />
</p>

10. If you would like to emphasize the BIM Model in the view, you can use the *Opacity* feature to control the background map.

<p align="center">
<img src="/images/bim_settings.png" width = 600 />
</p>

{{< hint danger >}}
Watch the full [Video](https://vimeo.com/680854025) for more information.
{{< /hint >}}

