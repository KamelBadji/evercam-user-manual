---
title: Requirements
weight: 4
---

# BIM Requirements

## File Type

- Clean single or multi-discipline Revit file (.rvt) 

- Federated Model in Navisworks (.nwd)

- Synchro Model (.sp)

{{< hint warning >}}
Note: The given model should preferably be the latest and final design model.
{{< /hint >}}

If you are using any other BIM software, you may share with us an IFC format or let us know which software you are using and we'll coordinate with you on how we can integrate.