---
title: FAQs
weight: 5
---

# BIM FAQs

### **What are the benefits of integrating our BIM Model?**
1. Improved Collaboration: BIM encourages interdisciplinary collaboration, enabling different 
teams to work using the same model simultaneously, reducing conflicts, and enhancing 
communication.

2. Visualization: BIM provides realistic 3D visualizations, helping stakeholders better 
understand the design and make informed decisions.

3. Cost and Time Efficiency: By improving coordination and reducing rework, BIM can help save 
costs and expedite construction schedules.

### **We update our BIM model frequently, how is it possible to get it updated on BIM?**
If you have the BIM Model in Bentley iModel® Hub, you just have to grant us (3d@evercam.io) 
admin access, then the update will be automatic.

If not, you can send us an updated BIM Model. We’ll update it on our side, then process it on 
our Bentley iModel® Hub (this takes 0.5 to 3 days depending on the model size).

### **Can you integrate with my 3D model, I don't have 4D?**
Yes, we can integrate both 3D and 4D on BIM.

### **What are the system requirements to run BIM on my laptop?**                                                                                       
BIM is a cloud-based, SaaS solution that is accessed via a standard web browser from any device 
with internet connectivity – no additional client software is required.

### **What are the construction softwares that you can integrate with?**                                                             
We can integrate with Revit, Navisworks, and Synchro. 