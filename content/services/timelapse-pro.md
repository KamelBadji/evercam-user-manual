---
title: Time-Lapse Pro
weight: 1
---
# Time-Lapse Pro

{{< hint info >}}
Time-Lapse Pro (TLP) is an add-on subscription providing monthly professionally edited 
time-lapse videos produced by the Evercam Video Editing team. Time-Lapse Creator (TLC) is 
available, regardless of a TLP subscription, and custom one-off milestone videos can be ordered at any time.
{{< /hint >}}

## Ordering & Customizing a Professionally Edited Video

1. Ordering:
    - TLP Monthly subscription: One professionally produced video/month.  Add **“TL Pro - Package A Add-On”** to your shopping cart via Coupa for monthly videos.
    - One-off: Order a one-off milestone video via **“Pro TL Video Add on”** (each) via Coupa.

2. Provide your CSM with video requests during the Project Kickoff Meeting.

3. For specific milestone video requests (that differ from standard monthly videos), please fill out this form
{{< button href="https://forms.zohopublic.com/Evercam/form/TimelapseFormV2/formperma/1Z5PiEzmFCLDG9K79mI0mAXxyaX8app_0QxuQer0MU4" >}}Time Lapse Request Form{{< /button >}}.

4. Please include any Custom Requests:
    - Add transitions
    - Custom text design according to clients brand guidelines
    - Remove time-stamp
    - Blur / Zoom in 
    - Hide privacy shields
    - Custom text requirements 
    - 3rd party images, Drone footage, photos, etc

5. Email video@evercam.io and/or your Customer Success Manager for questions/concerns about your video.
