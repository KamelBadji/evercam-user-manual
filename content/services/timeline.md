---
title: Timeline
weight: 2
---
# Timeline

{{< hint info >}}
A 4D BIM Model contains the project timeline which shows important construction milestones and can be used for tracking progress of a project.
{{< /hint >}}

<p align="center">
<img src="/images/bim_timeline.png" width = 600 />
</p>

## Features

- Visualise all project milestones.
- Jump from one milestone to another.
- The timeline will be streamed on both BIM and BIM Compare.

## Types of Timeline Creation

1. **4D Timeline Matching**
    - Combining an existing BIM model with an existing schedule (P6/Aconex/PDF/CSV)
    - Duration: 6 Business Days on average.

2. **4D Timeline Retrofitting**
    - Taking a bi-weekly or monthly snapshot to match the milestones.
    - The result will be a timeline that matches the actual construction progress.
    - Duration: 3 Business Days on average per construction year processed.

3. **4D Timeline Rebasing**
    - Fixing an erroneous schedule to match existing camera footage.
    - Duration: 15 Business Days on average.

## Requirements

- We require the original BIM file, preferably in Revit or Navisworks format, with EDIT 
privileges.
- A copy of the baseline project schedule (preferably an Excel file)
- A reference architectural site & floor plan showing the zoning/ blocking/ area codification


## Requesting for a Timeline

1. You can ask your Account Manager to coordinate with the 3D Team to include a timeline in BIM Compare and BIM.

2. Share the requirements stated above to the Evercam 3D Team.

3. Timelines take about 14 days minimum to be reflected in the project. If the timeline is already embedded within the BIM model, the lead time will be much less.

4. We will notify you once the timeline is ready for viewing.