---
title: Hardware
weight: 9
bookCollapseSection: true
---

# Hardware

## Camera Types

{{< columns >}}
1. Standard Fixed Position Mains Box

   <img src="/images/Mains.png" width = 200 />

<--->

2. PTZ

    <img src="/images/PTZ_camera.png" width = 200 />

    <--->

3. 180 ° Internal/ External

    <img src="/images/180_camera.png" width = 200 />

    <--->

4. ANPR Fixed

    <img src="/images/ANPR_fixed.png" width = 200 />

<--->

5. Gate Report-ANPR-7m Mast

    <img src="/images/GR-anpr-mast.png" width = 100 />

{{< /columns >}}

## Installation Configurations

{{< columns >}}
1. Solar Powered

    <img src="/images/Solar.png" width = 200 />

<--->

2. Mobile Solar Trailer (Tycon)

    <img src="/images/mobile_solar_trailer.png" width = 200 />

<--->

3. Non Penetrating Mount (Fishtank)

    <img src="/images/non-penetrating_mount.png" width = 200 />

<--->

4. Preconfigured Self-Install Kit

    <img src="/images/fixed_position_camera.png" width = 200 />

<--->

5. Cabin Mounted

    <img src="/images/cabin_mounted.png" width = 200 />

<--->

6. Mast Tilt-Down fixed

<img src="/images/Mast_tilt_down_fixed.png" width = 200 />

<--->

7. Mast 7m Portable

<img src="/images/Mast_7m_portable.png" width = 200 />

{{< /columns >}}