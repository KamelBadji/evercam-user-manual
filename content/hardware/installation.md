---
title: Installation
weight: 10
bookCollapseSection: false
---

# Installation 

## Powered Unit Self-Installation

<p align="center">
<img src="/images/powered_unit.jpg" width = 200 />
</p>

### Unpackage / Inventory

1. Your self-installation camera kit will typically arrive in one package.
2. Unpackage the contents of the box.
3. Inspect the equipment and compare it to the enclosed bill of materials (BOM).

### Installation

If the camera arrives already attached to the enclosure, proceed to the mounting section below.  If the camera does not arrive attached to the enclosure, follow the assembly steps below.

### Assembly

1. The camera conduit box has been attached to the enclosure by Evercam.
2. Attach the camera to the conduit box using a T10 Torx bit (provided inside the enclosure). 

<p align="center">
<img src="/images/conduit_box.jpg" width = 300 />
</p>

### Mounting

**Magnetically** (not recommended for outdoor locations)
1. The camera enclosure arrived with four (4) 65lb magnets attached.

<p align="center">
<img src="/images/magnets.jpg" width = 300 />
</p>

2. Locate a steel location for the camera kit to be mounted (column, beam, etc…)
3. Hold the enclosure next to the steel structure and allow the magnets to make contact.

**Traditional Fasteners**

1. Using a 9/16” or adjustable wrench remove the magnets.
2. Hold the enclosure to the wall in the desired location.  Level the enclosure.
3. Using a pencil, mark the four (4) mounting hole locations.
4. Using the proper wall anchors / fasteners for your wall material, mount the enclosure securely to the wall.

### Powering / Commissioning

1. Plug the enclosure power cord into the wall outlet.  The use of an extension cord (not provided by Evercam) may be needed to reach the outlet.
2. The camera system is pre-configured and will be activated upon powering up (the power up process takes a few minutes).
3. Once the system is powered, the camera is activated in the Evercam platform and you should be able to see the camera field of view (FOV).
4. Adjust the FOV by loosening the ring around the camera arm.  Once the desired FOV is achieved, tighten the ring down.

<p align="center">
<img src="/images/ring.jpg" width = 300 />
</p>

5. If needed, contact Evercam Remote Tech Support Contact on the support list.

## Pole Mount Solar Installation

### BBA-6 Enclosure Step-by-Step

1. Prep the enclosure by drilling holes in the enclosure bottom and dividing shelf per the following diagram for cable pass thru.  Insert (1) weatherproof RJ45 (cate5/cat6) connector, (1) PG16 cable gland, and (1) PVC snap in bushing

<p align="center">
<img src="/images/shelf.png" width = 300 />
</p>

<p align="center">
<img src="/images/bottom.png" width = 300 />
</p>

<p align="center">
<img src="/images/BBA-6.png" width = 500 />
</p>

2. Mount the BBA-6 enclosure using the center holes in the top & bottom mounting flanges.
   a. Measure and mark the pole at 6’-0” AFG.  This will be the top flange hole location
   b. Pre-drill using a 1/4” drill bit
3. Mount the BBA-6 enclosure using 3/8”x3 1/2” galvanized lag screws & 3/8” galvanized washers. Repeat for bottom flange.

<p align="center">
<img src="/images/BBA_mount.png" width = 500 />
</p>

4. Install (3) 110ah batteries into the bottom compartment of the enclosure.

<p align="center">
<img src="/images/110_batteries.jpg" width = 300 />
</p>

5. 	Mount the back panel into the top compartment of the enclosure.  The Panduit should line up above the 1 3/8” hole w/ the PVC snap in bushing.

<p align="center">
<img src="/images/panduit.jpg" width = 500 />
</p>

### Solar Panel Step-by-Step

1. Solar Panels must be installed facing a south-eastern direction.

<p align="center">
<img src="/images/southeast.png" width = 200 />
</p>

2. Mark the mounting hole location for the bottom solar panel bracket according to the below 
diagram.  Using a ¼” drill bit, pre-drill top and bottom solar bracket holes.

<p align="center">
<img src="/images/mounting_hole_diagram.png" width = 300 />
</p>

3. Attach the solar panel to the mounting bracket before mounting the panel / assembly mount to the pole.

<p align="center">
<img src="/images/mounting_bracket.png" width = 500 />
</p>

4. Mount the bottom solar panel / mount assembly to the pole using (2) 3/8”x3 1/2” galvanized lag bolts with 3/8” washers.

<p align="center">
<img src="/images/mount_solar_panel.png" width = 500 />
</p>

5. Repeat these steps for the remaining solar panel brackets / panels.

### Solar Panel Wire Terminations

Using the Evercam provided 5’-0” long MCA extension cables, connect the solar panels in “series” per the following diagram:

<p align="center">
<img src="/images/MCA_cables.png" width = 400 />
</p>

Connect the positive (+) and negative (-) ends of the 10awg solar cable to the appropriate PV terminals on the MPPT charge controller. 

### Battery Wire Terminations

Connect the battery negative (-) terminals together and then to the negative (-) BATT terminal on the MPPT charge controller.

Connect the battery positive (+) terminals together and then to the positive BATT terminal on the MPPT charge controller.

<p align="center">
<img src="/images/battery_wire_terminations.png" width = 500 />
</p>

## Roof-top Solar Installation

*(to be updated)*

## Non-penetrating Roof Mount (NPR) Installation

<p align="center">
<img src="/images/roof_mount.png" width = 300 />
</p>

### Unpackage & Assembly

1. Unpackage the non-penetrating roof mounts components from the manufacturer’s packaging.  
Confirm receipt of all components.  Organize the “L” shaped angle brackets per the following 
diagram ensuring that the “center” holes are aligned.

<p align="center">
<img src="/images/angle_brackets.jpg" width = 300 />
</p>

2. Using a ½” socket / ratcheting wrench, install the provided carriage bolt, flat washer, and 
nut at each connection point.  Insert and hand tighten all connectors prior to fully tightening 
them down.

<p align="center">
<img src="/images/socket_wrench.jpg" width = 300 />
</p>

3. Spread the legs of the tripod assembly and align with the base support per the following 
image ensuring that the 2” pipe support ring is centered above the center support holes.  
Attach using a ½” socket / ratcheting wrench, carriage bolt, flat washer, and nut at each 
connection point.

<p align="center">
<img src="/images/tripod.jpg" width = 500 />
</p>

4. With the pre-drilled holes down, insert the 2” mast through the support ring, aligning the 
holes with the holes in the center support of the base.  Connect the 2” mast to the support 
base using a ½” socket / ratcheting wrench, carriage bolt, flat washer, and nut at each 
connection point.

<p align="center">
<img src="/images/mast.png" width = 500 />
</p>

5. Tighten all connections including the pre-installed bolts on the support ring.

### Roof-top Installation

1. At the desired location, place the manufacturer provided rubber mat on the roof to protect 
roofing and place the non-penetrating roof mount assembly on the mat.

<p align="center">
<img src="/images/rubber_mat.png" width = 500 />
</p>

2. Insert six (6) standard concrete blocks inside the non-penetrating roof mount base to weigh 
down and stabilize the assembly.

<p align="center">
<img src="/images/rubber_mat.jpg" width = 500 />
</p>

### Enclosure Mounting

1. Using the provided hardware, mount the metal mounting brackets to the enclosure flange on 
both top and bottom.

<p align="center">
<img src="/images/enclosure_flange.png" width = 500 />
</p>

2. The enclosure could then be mounted to the 2” mast of the non-penetrating roof mount 
assembly using either pipe straps or U-bolts.

<p align="center">
<img src="/images/2in_mast.png" width = 500 />
</p>

### Camera Mounting

**Hikvision:**

1. Using the appropriate fasteners for your mounting location, attach the silver mounting plate to the wall with the arrow facing up.

<p align="center">
<img src="/images/2in_mast.jpg" width = 200 />
</p>

2. Using the provided T25 security bit, attach the conduit box to the mounting plate.

<p align="center">
<img src="/images/conduit_box_2.jpg" width = 200 />
</p>

3. Connect the steel cable tether from the backbox to the camera.

<p align="center">
<img src="/images/steel_cable.png" width = 500 />
</p>

4. Using the provided T25 security bit, attach the camera to the conduit backbox.  Remove 
protective film from lens cover.

<p align="center">
<img src="/images/lens_cover.png" width = 300 />
</p>

5. Using the provided T25 security bit, loosen the screw on the camera arm and adjust for 
desired field of view (FOV).  Tighten screw securely to avoid camera position slippage.

<p align="center">
<img src="/images/arm_screw.png" width = 300 />
</p>

**Milesight:**

*(to be updated)*

## Victron Connect

In order to perform any of the following functions, you will need the Victron Connect app 
downloaded to your smart phone.  Search for and download the app using your device’s app store.

<p align="center">
<img src="/images/victron_connect.png" width = 200 />
</p>

### Connect to the MPPT Charge Controller

1. Open the Victron Connect app and locate the MPPT charge controller in the device list.

<p align="center">
<img src="/images/device_list.png" width = 200 />
</p>

2. Enter the device bluetooth PIN when prompted. PIN should be 000000 (6 zeros).

<p align="center">
<img src="/images/victron_pin.png" width = 300 />
</p>

3. You are now connected to the MPPT charge controller and can perform the following operations:

### Firmware Upgrade:

1. If needed, your MPPT charge controller will prompt you to upgrade the device’s firmware.  
Click the update button.  Note: Do not leave this screen or take any incoming phone calls 
during the upgrade process as this will interrupt / cancel the upgrade.

<p align="center">
<img src="/images/firmware_upgrade.png" width = 400 />
</p>

2. Once the upgrade completes, click continue. 

<p align="center">
<img src="/images/firmware_update.png" width = 200 />
</p>

3. Your device’s firmware is now up to date.

### Battery Readings:

*(Connect to the MPPT charge controller using the Victron Connect app and bluetooth. Please ensure you are within range of the Evercam equipment enclosure.)*

1. Open the Victron Connect app. Locate the MPPT charge controller in device list.

<p align="center">
<img src="/images/device_list.png" width = 200 />
</p>

2. Choose the charge controller you want to connect to by clicking on the correct name from the 
list. This will open the device information page. The battery voltage reading is located 
approximately half down the screen.

<p align="center">
<img src="/images/battery_voltage.png" width = 200 />
</p>

### Load Output Settings

1. The load output settings are displayed towards the bottom of the device information screen.

<p align="center">
<img src="/images/load_output.png" width = 200 />
</p>

2. To change the load output state, click on the setting gear icon on the top right of the page

<p align="center">
<img src="/images/load_output_settings.png" width = 200 />
</p>

3. Choose the Load Output option.

<p align="center">
<img src="/images/load_output_option.png" width = 200 />
</p>

4. Switch the load output to “always on”, and then click the back arrow.

<p align="center">
<img src="/images/always_on.png" width = 200 />
</p>