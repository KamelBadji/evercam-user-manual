---
title: FAQs
weight: 11
---

# Hardware / Installation FAQs

### **What is the process of self installation? Is it difficult?**
The self-installation process is quite simple. The camera kit arrives packaged with the camera 
already mounted and connected to the enclosure. Once the hardware has been unpackaged, the  
enclosure will need to be mounted to the structure (wall, beam, column, pole, etc). The 
enclosure arrives with magnets already attached for a steel beam/column installation. After 
the enclosure is mounted, connect the ac power cord to the outlet or extension cord (not 
provided). The camera and associated hardware are preconfigured and will boot up / connect to 
the Evercam platform upon powering. Contact Evercam support staff to finalise the camera field 
of view (FOV).

### **What is lead time?**
- Self install Bullet:
- Self install 180:
- Pro Install Solar:

### **What is included in the Evercam system shipped to me?**
You will receive a camera, a mount, an enclosure (router, hard-drive, edge processor), and the 
necessary cabling. If you are using solar, solar components are included. 

### **Is the hardware weatherproof?**
Exterior camera systems have enclosures that are designed to protect against elements. Dirt, 
rain, dust may accumulate on the lens, which we’d recommend wiping off if it is obstructing the 
field of view. 
