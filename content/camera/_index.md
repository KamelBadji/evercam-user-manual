---
title: Cameras
weight: 2
bookCollapseSection: true
---

# Cameras

Evercam records every second of your construction project and unifies fixed-position cameras, 
4D models, drones & other reality capture data in one central location.

## Subtopics

- [Live View]({{< relref "/camera/live-view" >}})
- [Recordings]({{< relref "/camera/recordings" >}})
- [Time-lapse Creator]({{< relref "/camera/timelapse-creator" >}})
- [Sharing]({{< relref "/camera/sharing" >}})
- [Compare Tool]({{< relref "/camera/compare-tool" >}})
- [BIM Compare]({{< relref "/camera/bim-compare" >}})
- [Edit Tool]({{< relref "/camera/edit-tool" >}})
- [Digital Zoom]({{< relref "/camera/digital-zoom" >}})
- [Measuring Tool]({{< relref "/camera/measuring-tool" >}})
- [Redaction]({{< relref "/camera/redaction" >}})
- [PTZ]({{< relref "/camera/ptz" >}})