---
title: Time-Lapse Creator
weight: 6
bookCollapseSection: true
---

# Time-Lapse Creator

{{< hint info >}}
A tool to create your own construction time-lapse videos to improve project communications. 
{{< /hint >}}

<p align="center">
<img src="/images/timelapse.png" width = 600 />
</p>

This is the perfect way to capture an event that has happened during your project and quickly share that information internally or to your clients.

## How to Use

1. In the Project Dashboard, you can select which camera view you wish to make a time-lapse from.

2. Then go to the *Time-Lapse* tab that appears at the top.

3. You can select the specific Period and Schedule.
   
    - Period: Duration of video capture in calendar days
    - Schedule: Continuous, working hours, or custom

4. You can choose between 30, 60, and 90 seconds for the duration of your video.

<p align="center">
<img src="/images/timelapse_format.png" width = 600 />
</p>

5. You can also opt to remove the time stamp or provide smooth frame transitions.

<p align="center">
<img src="/images/timelapse_effects.png" width = 600 />
</p>

6. If you'd like to add your company logo, you can do so.

7. You can put a title on your video for easy identification, or leave it as is, showing the project and calendar captured.

<p align="center">
<img src="/images/timelapse_confirm.png" width = 600 />
</p>

8. Once the video is being made, you can close the window, or go directly to the *Archives* tab on the left sidebar.
   - New videos still being processed will have the Evercam Logo over them. 
   - Completed videos will display the time-lapse image.

9. On the *Archives*, you can: 
    - view the time-lapse in the window
    - rename the time-lapse
    - delete the time-lapse

10. Finally, you can share the time-lapse, by either: 
    - downloading the video and sharing it with others, or
    - clicking the *is public* icon, allowing sharing through social media and hyperlinks.

10. Clicking *Share* allows you to share via social media, as well as via two hyperlinks, 
one that takes you to the Dash, and the other to the source hosted video.
