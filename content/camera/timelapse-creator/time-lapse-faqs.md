---
title: FAQs
weight: 12
---

# Time-Lapse FAQs

### What is the difference between footage from Time-Lapse Creator (TLC) vs Cloud Recordings in the evercam platform?
Recordings are limited to 1 hour captures at a much higher frame rate for review of work and 
incidents, without the request of the full frame rate video from Evercam Support. 

Time-Lapse Creator (TLC) is focused on team collaboration across longer durations and to show 
project progress.

### How do I request a special video from Evercam’s video editing team?
The preferred method is to submit this form with your request - [Time-Lapse Video Request Form](https://forms.zohopublic.com/Evercam/form/TimelapseFormV2/formperma/1Z5PiEzmFCLDG9K79mI0mAXxyaX8app_0QxuQer0MU4).

Or you can email video@evercam.io with details of the video request in the email body (time line, special instructions, 3rd party footage, etc).

### How do I receive my monthly video? Where can i find it later?
Monthly professionally edited videos are emailed to all users on a project. Videos can also be 
accessed at any time in Archives. 

### What does Time-Lapse Pro (TLP) include as an add-on to my project?
One Monthly Professionally Edited Time-Lapse Video - Customizable.

### What custom capabilities does Evercam’s Video Team offer with Professional videos?
1. Cleaning The Footage
    - Removed Bad Weather and Blurred Images
    - Footage Stabilisation
    - Colour Correction
2. Graphic Design
    - Project Name, Date, Logo
    - Logo Animations to open and close the video
    - Custom Text Requirements
    - Custom Text Design according to Brand Guidelines
3. Combined Video Sources
    - Multiple cameras
    - Drone footage
    - BIM Integrations
4. Custom Requirements
    - Adding Transitions
    - Crop, Zoom, Frame the Video
    - Blurring Areas
    - Hide Privacy Shields

### What Time-Lapse tools are included in the standard software? What Time-Lapse tools do I get if I dont purchase TLP as an add-on?
Time-Lapse Creator and one End of Project professional video production is included with the 
standard offering. If you’d like a monthly time-lapse produced by our video editing team, 
you’ll need to add TLP to your project order.

### How does the Time-Lapse Creator work? How can I create my own Time-lapse in minutes on the evercam platform?
We’ve created a tutorial for use: [Time-Lapse Creator Tutorial](https://docs.google.com/presentation/d/1IiHw1NLNB2E12jHS5i8hml7_j8jbw_3ybEgQXkjlCzQ/edit#slide=id.g140315ae2ad_0_109) 

### I just got my monthly video but realised I want a special video, can I request changes to make it special?
You can use next month’s credit to generate a new special video. 

### Can I cancel my “Time-Lapse Pro” subscription at any time?
With one (1) month’s notice, you can cancel your “Time-Lapse Pro” subscription, but you 
cannot restart it again for any less than 3 month’s time. 

### With the “Time-Lapse Pro” subscription, can I choose not to receive a video this month and get 2 videos next month?
You receive one video each month. If a custom video is not requested, you will receive a 
professionally completed monthly progress update video. We cannot roll out multiple videos in the same month, but you can make your video request from footage from a prior month.
