---
title: Live View
weight: 4
---

# Live View

{{< hint info >}}
Shows the real-time view of a project from a camera.
{{< /hint >}}

<p align="center">
<img src="/images/live_view.png" width = 600 />
</p>

Site visibility is very important. Evercam provides you with a live view of your construction site that can be accessed any time, anywhere using your mobile, laptop, or tablet from the office, on the road, or even onsite.

## How to Use

1. On *Live View* in the dashboard, you can zoom in or out on any area of the camera view.

2. If you want to access weather information for the current date or past dates, click on the *Weather* icon at the lower right hand side.

3. If you want to share the image, use the *Edit* tool to take a screenshot of the live view and add drwaings or notes as needed.

4. You can download the image to *Archives*.

5. You can choose to *Pause* the live view when you please. 

**Note: Live View will automatically pause after a few mins of inactivity. Just click resume to play the live view.**

<p align="center">
<img src="/images/live_view_paused.png" width = 600 />
</p>