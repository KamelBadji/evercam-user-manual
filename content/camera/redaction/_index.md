---
title: Redaction
weight: 14
---

# Redacted Camera

{{< hint info >}}
A copy of the Project Cameras but only shows recordings, that have been processed in real-time 
to remove fast moving objects (like human figures or cars) for privacy purposes. 
{{< /hint >}}

It can also be used to make the construction site clear of humans which can be useful for 
project progress timelapse.

On the Project Dashboard, the camera list will show the actual cameras of the project 
plus copies of each camera with the word *redacted* attached to them. 

## Original Camera View

<p align="center">
<img src="/images/original_camera.png" width = 600 />
</p>

## Redacted Camera View

<p align="center">
<img src="/images/redacted_camera.png" width = 600 />
</p>

**Note: Ask your Account Manager if you would like to add Redacted Cameras to your project.**