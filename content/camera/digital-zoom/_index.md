---
title: Digital Zoom
weight: 12
---

# Digital Zoom

{{< hint info >}}
A tool that catches every detail in your project up to 18X beyond the basic zoom level.
{{< /hint >}}

<p align="center">
<img src="/images/digital_zoom.png" width = 600 />
</p>

Being able to zoom into details enhances site visibility on a virtual level. 

## How to Use

1. You must be within the *Live View* or *Recordings* tab to use this tool.

2. You can use the mouse wheel or select the (+)/(-) symbols on the lower left of the viewer to 
zoom in or out.

3. Drag and click on the specific area that you want to inspect.