---
title: Sharing
weight: 7
---

# Share your Camera/Project

{{< hint info >}}
Users with “Full Rights” and “Read + Share” can share cameras or projects with unlimited users.
{{< /hint >}}

<p align="center">
<img src="/images/Share_project.png" width = 800 />
</p>

We do not limit the number of users who can view a camera or a project. We encourage collaboration and communication among all stakeholders of the project.

## How to Use

1. You can click on the *Sharing* tab along the top of the dashboard.

**Note**: If you have been given *Read Only* permission, you will not be able to access this tab.

2. You can then enter the email addresses and a short message to the contacts you would like to 
share the camera with.

3. You can choose between *Full Rights*, *Read + Share*, or *Read Only* rights.

<p align="center">
<img src="/images/permissions.png" width = 300 />
</p>

- Full Rights: (Project Admin) this permission allows the user to access network details, change username and password, change user permissions, aside from access all features of the platform and share the cameras with other people.

- Read + Share: this permission allows the user to access all features of the platform and share the cameras with other people.

- Read Only: this permission allows the user to view and access all features of the platform but **cannot** share the cameras with other people.

4. Alternatively, feel free to send us a list of all of the email addresses you would 
like to add to the camera, and we will take care of it for you.

{{< hint danger >}}
Watch the [Full sharing Tutorial](https://evercam.io/tutorials/sharing-the-camera/) for more information.
{{< /hint >}}