---
title: Measuring Tool
weight: 13
---

# Measuring Tool

{{< hint info >}}
A 2D grid overlaid on top of the camera live view allowing the user to measure distances between two points. 
{{< /hint >}}

<p align="center">
<img src="/images/Measuring_grid.png" width = 600 />
</p>

## Use Cases

- You can save time by measuring remotely without having to go to the construction site.
- You can do daily checks & monitor progress through a lens.
- You can estimate the quantity of equipment and materials.
- You can verify spacing and gaps.

## Requirements

- BIM Model

- Camera :
    - Calibrated
    - Installation Height >10m, looking down

- Landmarks (common to the BIM and the construction site on Camera view)

## Limitations

- It is not possible to be used at the beginning of a construction site without any landmark.
- It is not possible to be used on several floors. 
- It is not possible to be used for uncalibrated camera/ Camera that is not high enough
- It can’t measure height.

# How to Use

1. You have to be in eiother *Live View* or the *Recordings* tab to access the measuring tool.

2. If this is enabled for your project, you will see a ruler icon on the lower right side of the view.

3. You can choose between *Meters* and *Feet and Inches*.

4. You can use the transparency function at the top left corner to emphasize or hide the grid.

5. To start measuring, just click on any two points.

*In most cases, one square on the grid is equal to 1 meter, but sometimes the camera distortion doesn't allow for it so we end up with 1.5 or 2m or even more. This can be checked with the 3D Team.*

**Note: This is an add-on. Should you want to add it to your project, pls. coordinate with your Account Manager.**


