---
title: Edit Tool
weight: 11
---

# Edit Tool

{{< hint info >}}
A tool that allows you to communicate visually by drawing and adding text to images 
of your construction site.
{{< /hint >}}

<p align="center">
<img src="/images/edit_tool.png" width = 500 />
</p>

This tool enhances collaboration and team communications at the comfort of your desk.

## How to Use

1. To use the *Edit* tool, first go to *Live* view or *Recordings* and then select the Edit icon on the lower right of the viewer.

2. You can use our full range of Edit Tools to highlight what’s important to you or what needs to be communicated to others.

<p align="center">
<img src="/images/edit.png" width = 500 />
</p>

3. If you want to discard your edited image, just select *Back*.

4. You can change the name of the image using the *Pencil* icon.

5. You can either *Download* the image to your computer, *Send* it to Procore/Aconex (if you use any of those platforms), or *Save* the image to your Project Archives.

<p align="center">
<img src="/images/edit_image_options.png" width = 500 />
</p>

{{< hint danger >}}
Watch the [Edit Tool Video Tutorial](https://vimeo.com/577961132) for more information.
{{< /hint >}}