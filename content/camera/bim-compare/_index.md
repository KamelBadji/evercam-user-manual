---
title: BIM Compare
weight: 10
---

# BIM Compare

{{< hint info >}}
A tool used to compare how the project looks as-designed vs. as-built by showing an image of the BIM Model overlaid on top of the camera live view.
{{< /hint >}}

<p align="center">
<img src="/images/bim_compare.png" width = 600 />
</p>

**What is BIM (Building Information Modelling)?**

- BIM is more than a 3D Model: It’s about building <mark>INFORMATION</mark>.
- It contains information about construction elements, measurements, materials, suppliers and, 
most importantly, the construction schedule.

For the BIM Compare feature to be enabled, you have to give us a copy of the BIM Model which we will use to align with the camera live view. Once the model has been aligned, we will notify you that the feature has been enabled.

**Requirements**

- A Geolocated BIM Model in any popular format (e.g. Revit, Synchro, Navisworks). For best 
results, include the project 4D timeline.
- The camera coordinates (position on site) provided by surveyor.

<p align="center">
<img src="/images/bim_formats.png" width = 600 />
</p>


**Works with any FoV (Field of Vision)**

<p align="center">
<img src="/images/bim_fov.png" width = 600 />
</p>

**Layers**

Different layers of the BIM model can be overlaid on the BIM compare feature and you can switch 
between them.

<p align="center">
<img src="/images/bim_layers.png" width = 600 />
</p>

## Features

<p align="center">
<img src="/images/bim_compare_db.gif" width = 600 />
</p>

1. If you want to access the features, click on the *BIM Compare* tab along the top of the dashboard.

2. If you want to view a specific date, select the *Camera View* on the side panel and use the calendar to choose the date and time.

3. Then you can use the BIM calendar to choose the date of the 4D model to match the Camera View date. Alternatively, you can use the timeline on the bottom of the screen to view specific BIM Model dates.

4. In order to compare the BIM Model with the construction progress, click and drag the slider to the left or right.

5. If you want to view a specific model layer, you can select the layer (Architectural/ Structural/ MEP/ 4D) in the side panel. *Note:* Available layers are dependent on the BIM model given.

6. If you would like to see the live view behind the BIM Model, you can use the *Transparency* tool on the side panel.

<p align="center">
<img src="/images/bim_compare_tools.png" width = 600 />
</p>

7. If you would like to download or share the image, you can export the current view as a jpeg. The saved image can be found in *Archives*.


{{< hint danger >}}
Watch this [Tutorial](https://vimeo.com/showcase/8254136/video/515229597 ) for more info.
{{< /hint >}}

{{< hint warning >}}
If the camera moves from its original position, the BIM Model will no longer be correctly aligned. We will re-align it when we are notified of the misalignment.
{{< /hint >}}