---
title: Compare Tool
weight: 9
---

## Compare Tool

{{< hint info >}}
A tool to compare before and after images from any point in time with the ability to export them into GIFs.
{{< /hint >}}

<p align="center">
<img src="/images/compare.png" width = 600 />
</p>

This tool enables users to track progress on site and use the images for documentation and presentations.

## How to Use

1. To start using this tool, click on the *Compare* tab along the top of the dashboard. 

2. You can choose the *Before* and *After* date.

3. You can choose to Share, Download, or Embed the current view by clicking the *Export As* button.

4. You'll need to place a title and then click *Save*.

<p align="center">
<img src="/images/compare_confirm.png" width = 400 />
</p>

<p align="center">
<img src="/images/confirm_step_2.png" width = 400 />
</p>

5. You can download the Compare as an animated .gif, .mp4 file or embed the code onto your website.

<p align="center">
<img src="/images/confirm_step_3.png" width = 400 />
</p>

{{< hint danger >}}
Watch the [Compare Tool Tutorial Video](https://vimeo.com/400597609) to find out more.
{{< /hint >}}